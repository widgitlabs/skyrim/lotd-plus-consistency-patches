$CCF_Clothing Fixes	Clothing Fixes
$CCF_ClothingClutterFixes	Clothing & Clutter Fixes
$CCF_OutfitOptions	Outfit Options
$CCF_JarlsStewards	Jarls & Stewards
$CCF_CourtWizards	Court Wizards
$CCF_CollegeMages	College Mages
$CCF_PriestsPriestesses	Priests & Priestesses
$CCF_OtherMages	Other Mages
$CCF_Dunmer	Dark Elves
$CCF_Children	Children
$CCF_Miscellaneous	Miscellaneous
$CCF_OtherOptions	Other Options
$CCF_ProtectiveClothing	Protective Clothing
$CCF_DescJarls	Allow this mod to change the outfits worn by Jarls and their stewards and housecarls.  New outfits will better reflect their station and allegiance.  Many of these changes are dynamic and will be applied as the civil war quest progresses.
$CCF_DescPriests	Allow this mod to change the outfits worn by priests and priestesses, giving them jewelry and robes that more properly reflect the divinity they serve.
$CCF_DescChildren	Allow this mod to change the outfits worn by certain children, giving them clothing that better reflect their life and role.
$CCF_DescWizards	Allow this mod to change the outfits worn by court wizards, giving them mage robes that better reflect their station and specialized school of magic.
$CCF_DescCollege	Allow this mod to change the outfits worn by NPCs associated with the College of Winterhold.  New outfits will better reflect their rank and school of magic.
$CCF_DescOtherMages	Allow this mod to change the outfits worn by other robed NPCs.  New outfits will better reflect their background and associations.
$CCF_DescDunmer	Allow this mod to change the outfits worn by certain Dunmer found in Skyrim.  Select Dark Elves will now be wearing Dunmeri tunics.
$CCF_DescMisc	Allow this mod to change the outfits worn by other NPCs.
$CCF_DescProtectiveClothing	Enabling this option will add a small armor value to worn clothing.  This will not count as armor or interfere with mage perks.  Applies ONLY to Player.
